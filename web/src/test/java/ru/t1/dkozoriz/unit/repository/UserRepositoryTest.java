package ru.t1.dkozoriz.unit.repository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.marker.UnitCategory;
import ru.t1.dkozoriz.tm.configuration.DataConfiguration;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.repository.ProjectRepository;
import ru.t1.dkozoriz.tm.repository.UserRepository;
import ru.t1.dkozoriz.tm.util.UserUtil;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataConfiguration.class})
@Category(UnitCategory.class)
public class UserRepositoryTest {

    private final User user = new User();

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test_user", "test_user");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        user.setLogin("test_login");
        user.setPasswordHash("test_password");
        userRepository.save(user);
    }

    @After
    public void clean() {
        userRepository.deleteById(user.getId());
    }

    @Test
    public void findByLoginTest() {
        final User test_user1 = userRepository.findByLogin("test_login");
        final User test_user2 = userRepository.findById(test_user1.getId()).orElse(null);
        Assert.assertEquals(test_user1.getId(), test_user2.getId());
        Assert.assertEquals(test_user1.getLogin(), test_user2.getLogin());
        Assert.assertEquals(test_user1.getPasswordHash(), test_user2.getPasswordHash());
    }

}