package ru.t1.dkozoriz.unit.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.marker.UnitCategory;
import ru.t1.dkozoriz.tm.configuration.DataConfiguration;
import ru.t1.dkozoriz.tm.model.Task;
import ru.t1.dkozoriz.tm.service.TaskService;
import ru.t1.dkozoriz.tm.util.UserUtil;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataConfiguration.class})
@Category(UnitCategory.class)
public class TaskServiceTest {

    private final Task task1 = new Task("Test Task 1", "description 1");

    private final Task task2 = new Task("Test Task 2", "description 2");

    @Autowired
    private TaskService taskService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test_user", "test_user");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        taskService.save(UserUtil.getUserId(), task1);
        taskService.save(UserUtil.getUserId(), task2);
    }

    @After
    public void clean() {
        taskService.deleteAll(UserUtil.getUserId());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, taskService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void findByIdTest() {
        final Task task = taskService.findById(UserUtil.getUserId(), task1.getId());
        Assert.assertEquals(task.getId(), task1.getId());
        Assert.assertEquals(task.getName(), task1.getName());
        Assert.assertEquals(task.getDescription(), task1.getDescription());
        Assert.assertEquals(task.getStatus(), task1.getStatus());
        Assert.assertEquals(task.getUserId(), task1.getUserId());
        Assert.assertEquals(task.getCreated(), task1.getCreated());
    }

    @Test
    public void addTest() {
        Task task = new Task("test_task", "test_description");
        task.setUserId(UserUtil.getUserId());
        taskService.save(UserUtil.getUserId(), task);
        Assert.assertEquals(3, taskService.count(UserUtil.getUserId()));
    }

    @Test
    public void updateTest() {
        Task task = new Task("test_task", "test_description");
        task.setUserId(UserUtil.getUserId());
        Assert.assertNull(taskService.update(UserUtil.getUserId(), task));
        taskService.save(UserUtil.getUserId(), task);
        task.setName("new_name");
        taskService.update(UserUtil.getUserId(), task);
        Assert.assertEquals("new_name", taskService.findById(UserUtil.getUserId(), task.getId()).getName());
    }

    @Test
    public void createTest() {
        taskService.create(UserUtil.getUserId(), "test_task", "test_description");
        Assert.assertEquals(3, taskService.count(UserUtil.getUserId()));
    }

    @Test
    public void countTest() {
        Assert.assertEquals(taskService.findAll(UserUtil.getUserId()).size(), taskService.count(UserUtil.getUserId()));
    }

    @Test
    public void deleteByIdTest() {
        taskService.deleteById(UserUtil.getUserId(), task1.getId());
        Assert.assertEquals(1, taskService.count(UserUtil.getUserId()));
    }

    @Test
    public void deleteAllTest() {
        taskService.deleteAll(UserUtil.getUserId());
        Assert.assertEquals(0, taskService.count(UserUtil.getUserId()));
    }

}