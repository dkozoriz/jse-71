package ru.t1.dkozoriz.tm.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.tm.enumerated.RoleType;
import ru.t1.dkozoriz.tm.model.Role;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.repository.UserRepository;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    @Autowired
    private final UserRepository repository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User findByLogin(final String login) {
        return repository.findByLogin(login);
    }

    public void createUser(final String login, final String password, final RoleType roleType) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        final String passwordHah = passwordEncoder.encode(password);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHah);
        final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        repository.save(user);
    }

    public void initUser(final String login, final String password, final RoleType roleType) {
        final User user = repository.findByLogin(login);
        if (user != null) return;
        createUser(login, password, roleType);
    }

    public List<User> findAll() {
        return repository.findAll();
    }

    @Transactional
    public User save(User user) {
        return repository.save(user);
    }

    @Transactional
    public User update(User user) {
        if (!repository.existsById(user.getId())) return null;
        return repository.save(user);
    }

    public User findById(final String id) {
        return repository.findById(id).orElse(null);
    }

    public long count() {
        return repository.count();
    }

    @Transactional
    public void deleteById(final String id) {
        repository.deleteById(id);
    }

    @Transactional
    public void deleteAll() {
        repository.deleteAll();
    }


    @Transactional
    public void lockUser(final String id) {
        final User user = findById(id);
        user.setLocked(true);
        update(user);
    }

    @Transactional
    public void unlockUser(final String id) {
        final User user = findById(id);
        user.setLocked(false);
        update(user);
    }

}