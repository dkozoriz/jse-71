package ru.t1.dkozoriz.tm.endpoint.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.t1.dkozoriz.tm.api.ProjectEndpoint;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.service.ProjectService;
import ru.t1.dkozoriz.tm.util.UserUtil;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/project")
public class ProjectRestEndpointImpl implements ProjectEndpoint {

    private final ProjectService projectService;

    @Override
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/getAll")
    public List<Project> getAll() {
        return projectService.findAll(UserUtil.getUserId());
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/count")
    public Long count() {
        return projectService.count(UserUtil.getUserId());
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/get/{id}")
    public Project get(
            @PathVariable("id") String id
    ) {
        return projectService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/post")
    public Project post(
            @RequestBody Project project) {
        return projectService.save(UserUtil.getUserId(), project);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @PutMapping("/put")
    public Project put(
            @RequestBody Project project
    ) {
        return projectService.update(UserUtil.getUserId(), project);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @DeleteMapping("/delete/{id}")
    public void delete(
            @PathVariable("id") String id
    ) {
        projectService.deleteById(UserUtil.getUserId(), id);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        projectService.deleteAll(UserUtil.getUserId());
    }

}