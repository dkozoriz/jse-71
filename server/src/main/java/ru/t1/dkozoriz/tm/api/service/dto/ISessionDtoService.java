package ru.t1.dkozoriz.tm.api.service.dto;

import ru.t1.dkozoriz.tm.dto.model.SessionDto;

public interface ISessionDtoService extends IUserOwnedDtoService<SessionDto> {

}