package ru.t1.dkozoriz.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.dkozoriz.tm.dto.model.AbstractModelDto;

@NoRepositoryBean
public interface AbstractDtoRepository<T extends AbstractModelDto> extends JpaRepository<T, String> {

}