package ru.t1.dkozoriz.tm.service.model.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.tm.api.service.model.business.IBusinessService;
import ru.t1.dkozoriz.tm.comparator.CreatedComparator;
import ru.t1.dkozoriz.tm.comparator.NameComparator;
import ru.t1.dkozoriz.tm.comparator.StatusComparator;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.NameEmptyException;
import ru.t1.dkozoriz.tm.exception.field.StatusEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;
import ru.t1.dkozoriz.tm.repository.model.business.BusinessRepository;
import ru.t1.dkozoriz.tm.service.model.UserOwnedService;

import java.util.List;

@Service
public abstract class BusinessService<T extends BusinessModel> extends UserOwnedService<T> implements IBusinessService<T> {

    @NotNull
    protected abstract BusinessRepository<T> getRepository();

    @Override
    @NotNull
    @Transactional
    public T changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final T model = findById(userId, id);
        if (model == null) throw new EntityException(getName());
        model.setStatus(status);
        update(userId, model);
        return model;
    }

    @Override
    @Nullable
    public List<T> findAll(@Nullable final String userId, @Nullable final ru.t1.dkozoriz.tm.enumerated.Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        if (sort.getComparator() == CreatedComparator.INSTANCE) return getRepository()
                .findAllByUserId(userId, Sort.by(Sort.Direction.ASC, "Created"));
        if (sort.getComparator() == NameComparator.INSTANCE) return getRepository()
                .findAllByUserId(userId, Sort.by(Sort.Direction.ASC, "Name"));
        if (sort.getComparator() == StatusComparator.INSTANCE) return getRepository()
                .findAllByUserId(userId, Sort.by(Sort.Direction.ASC, "Status"));
        return findAll(userId);

    }

    @Override
    @NotNull
    @Transactional
    public T updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final T model = findById(userId, id);
        if (model == null) throw new EntityException(getName());
        model.setName(name);
        model.setDescription(description);
        update(model);
        return model;
    }

}