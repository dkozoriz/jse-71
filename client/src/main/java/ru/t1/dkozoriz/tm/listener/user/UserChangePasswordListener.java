package ru.t1.dkozoriz.tm.listener.user;

import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class UserChangePasswordListener extends AbstractUserListener {

    public UserChangePasswordListener() {
        super("change-user-password", "change password of current user.");
    }

    @Override
    @EventListener(condition = "@userChangePasswordListener.getName() == #consoleEvent.name")
    public void handler(ConsoleEvent consoleEvent) {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        userEndpoint.userChangePassword(new UserChangePasswordRequest(getToken(), password));
    }

}